#!/usr/bin/python
'''
Meetup RSVP collection and stats.  Collect streaming RSVP events off the HTTP feed, and generate some basic statistics

For python 2.7, will require the following packages:

  pip install requests
  pip install pandas

Single argument of time to run, in seconds.  Default is 60 sec.
  e.g. from command line, run:

  ./Meetup.py 90

'''


import json
import requests
import signal
import sys

import pandas as pd
from datetime import datetime

# timeout signal, to physically timeout the stream.  requests.timeout will not stop the stream of events
class TimeoutException(Exception):
    """ Simple Exception to be called on timeouts. """
    pass

def _timeout(signum, frame):
    # Raise TimeoutException with system default timeout message
    raise TimeoutException()

def main():
    print('Meetup Stream Stats')
    print('-------------------')
    
    # Default timeout value - set to arg if provided
    streamTimeout = 60
    if len(sys.argv) > 1:
        streamTimeout = int(sys.argv[1])
    
    try:
        # Set the handler for the SIGALRM signal:
        signal.signal(signal.SIGALRM, _timeout)
        # Send the SIGALRM signal in 10 seconds:
        signal.alarm(streamTimeout)
    
        # Events (list of dicts)
        lines=[]
    
        print('Collecting RSVP events, for %i sec ...' % streamTimeout)
        print('')
        # Connect, and collect events for the streamTimeout period
        r = requests.get('http://stream.meetup.com/2/rsvps', stream=True, verify=False)
        if r.encoding is None:
            r.encoding = 'utf-8'
        for line in r.iter_lines(decode_unicode=True):
            if line:
                #print(json.loads(line))
                lines.append(json.loads(line))
                
    except TimeoutException:
        print('')
        print('Finished!')
    except Exception as e:
        print(e)
        r.close()
    finally:
        # Close the request if needed, and abort the sending of the SIGALRM signal:
        if r:
            r.close()
        signal.alarm(0)
    
    print('Total Number of Meetup RSVP events read: %i' % len(lines))
    
    # convert collected events (dicts) to a dataframe
    linesDF = pd.DataFrame(lines)
    #linesDF.shape
    
    # we are only interested in the events and the groups.  Convert event/group column dicts to separate DF's
    eventsSeries = linesDF['event']
    eventsDF = pd.DataFrame(eventsSeries.to_list())
    
    groupsSeries = linesDF['group']
    groupsDF = pd.DataFrame(groupsSeries.to_list())
    
    # The top 3 number of RSVPs received per Event host-country
    topEventCountriesSeries = groupsDF.groupby('group_country')['group_country'].count().nlargest(3)
    print('')
    print('Top Country RSVP Emitter Stats')
    print(topEventCountriesSeries)
    
    # String them together for final output stream
    topCountriesOutput = ''
    for i in topEventCountriesSeries.index:
        topCountriesOutput = topCountriesOutput + ',' + str(i) + ',' + str(topEventCountriesSeries[i])
        #print(i, topEventCountriesSeries[i])
    
    # get the timestamp and url for event furthest in future
    latestEventDF = eventsDF.groupby('time').max().tail(1)
    print('')
    print('Furthest RSVP event in future:')
    print(latestEventDF)
    
    latestEventUnixTime = latestEventDF.index.asi8[0] / 1000
    latestEventTime = str(datetime.utcfromtimestamp(latestEventUnixTime).strftime('%Y-%m-%d %H:%M'))
    
    latestEventUrl = str(latestEventDF.iloc[0]['event_url'])
    #print('Latest event timestamp: %s  URL: %s' % (latestEventTime, latestEventUrl))
    
    outputString = str(len(lines)) + ',' + latestEventTime + ',' + latestEventUrl + topCountriesOutput
    print('')
    print('Final Output String')
    print('-------------------')
    print(outputString)
    print('-------------------')

if __name__ == "__main__":
    main()
